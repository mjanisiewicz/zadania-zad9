# -*- coding: utf-8 -*-

from docutils.core import publish_parts
import re

from pyramid.httpexceptions import HTTPFound
from pyramid.view import view_config

from .models import Blog, Post


# Główna strona
@view_config(context='.models....', renderer='templates/list.pt')
def view_blog(context, request):
    """ Wyświetla listę n ostatnich wpisów """
    pass


@view_config(context='.models....', renderer='templates/view.pt')
def view_post(context, request):
    """ Wyświetla szczegółowo dany wpis """
    pass


@view_config(name='add', context='.models....')
def add_post(context, request):
    """ Dodaje nowy wpis """
    pass